import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

// Create a Form widget.
class Body extends StatefulWidget {
  @override
  BodyState createState() {
    return BodyState();
  }
}

class BodyState extends State<Body> {
  final _formKey = GlobalKey<FormState>();
  final searchFieldController = TextEditingController();

  void searchHandler() async {
    var url = 'https://www.google.com/search?q=${searchFieldController.text}';
    print(url);
    if (_formKey.currentState.validate()) {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(
          left: 40,
          top: 20,
          right: 40,
          bottom: 20,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text("Search into google", style: TextStyle(fontSize: 22)),
              SizedBox(height: 40),
              TextFormField(
                controller: searchFieldController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Search text',
                ),
              ),
              SizedBox(height: 20),
              RaisedButton(
                onPressed: searchHandler,
                child: Text('Search', style: TextStyle(color: Colors.white)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
