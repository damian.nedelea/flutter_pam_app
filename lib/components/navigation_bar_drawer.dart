import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RouteItem {
  String title;
  String route;

  RouteItem(this.title, this.route);
}

class NavigationBarDrawer extends StatelessWidget {
  String getCurrentRouteName(context) {
    String currentRouteName;

    Navigator.popUntil(context, (route) {
      currentRouteName = route.settings.name;
      return true;
    });

    return currentRouteName;
  }

  List<RouteItem> routeItems = [
    RouteItem("", ""),
    RouteItem("Google search", "/"),
    RouteItem("Camera", "/camera"),
    RouteItem("Push notification", "/notification"),
    RouteItem("Calendar", "/calendar"),
    RouteItem("Calendar events", "/calendar-events")
  ];

  @override
  Widget build(BuildContext context) {
    String currentRoute = this.getCurrentRouteName(context);
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView.builder(
        itemCount: this.routeItems.length,
        itemBuilder: (context, index) {
          if (index == 0) {
            return DrawerHeader(
              child: Text(
                'Drawer Header',
                style: TextStyle(color: Colors.white),
              ),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            );
          } else {
            return ListTile(
              title: Text(this.routeItems[index].title),
              selected: currentRoute == this.routeItems[index].route,
              onTap: () {
                if (currentRoute == this.routeItems[index].route) return;

                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pushReplacementNamed(
                    context, this.routeItems[index].route);
              },
            );
          }
        },
      ),
    );
  }
}
