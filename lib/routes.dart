import 'package:flutter/widgets.dart';
import 'package:flutter_pam_app/screens/calendar/calendar.dart';
import 'package:flutter_pam_app/screens/calendar_events/calendar_events.dart';
import 'package:flutter_pam_app/screens/camera/camera.dart';
import 'package:flutter_pam_app/screens/google_search/google_search.dart';
import 'package:flutter_pam_app/screens/push_notification/push_notification.dart';

import 'screens/camera/camera.dart';
import 'screens/google_search/google_search.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  "/": (BuildContext context) => GoogleSearch(),
  "/camera": (BuildContext context) => Camera(),
  "/notification": (BuildContext context) => PushNotification(),
  "/calendar": (BuildContext context) => Calendar(),
  "/calendar-events": (BuildContext context) => CalendarEvents(),
};
