import 'package:flutter/material.dart';
import 'package:flutter_pam_app/components/navigation_bar_drawer.dart';
import 'components/body.dart';
import 'calendar-bloc.dart';
import 'package:flutter_pam_app/bloc/bloc-prov.dart';

class Calendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  ExampleBloc exampleBloc;

  @override
  void initState() {
    super.initState();

    exampleBloc = ExampleBloc();
  }

  @override
  void dispose() {
    exampleBloc.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: exampleBloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Calendar"),
        ),
        body: Body(),
        drawer: NavigationBarDrawer(),
      ),
    );
  }
}
