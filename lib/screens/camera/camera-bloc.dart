import 'dart:async';
import 'dart:ui';
import 'package:flutter_pam_app/bloc/bloc.dart';

class CameraBloc extends Bloc {
  StreamSubscription _audioPlayerStateSubscription;

  Stream<String> get example => _exampleSubject.stream;
  Sink<String> get exampleSink => _exampleSubject.sink;
  final StreamController<String> _exampleSubject = StreamController<String>();

  CameraBloc();

  void dispose() {
    _exampleSubject.close();  
  }
}
