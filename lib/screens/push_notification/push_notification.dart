import 'package:flutter/material.dart';
import 'package:flutter_pam_app/components/navigation_bar_drawer.dart';
import 'components/body.dart';
import 'push_notification-bloc.dart';

import 'package:flutter_pam_app/bloc/bloc-prov.dart';

class PushNotification extends StatefulWidget {
  @override
  _PushNotificationState createState() => _PushNotificationState();
}

class _PushNotificationState extends State<PushNotification> {
  ExampleBloc exampleBloc;

  @override
  void initState() {
    super.initState();

    exampleBloc = ExampleBloc();
  }

  @override
  void dispose() {
    exampleBloc.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: exampleBloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Google search"),
        ),
        body: Body(),
        drawer: NavigationBarDrawer(),
      ),
    );
  }
}
