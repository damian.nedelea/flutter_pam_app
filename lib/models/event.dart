// Copyright (c) 2015, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'package:json_annotation/json_annotation.dart';

part 'event.g.dart';

@JsonSerializable()
class Event {
  @JsonKey()
  final String id;
  @JsonKey()
  final String name;

  @JsonKey()
  final DateTime startDate;

  @JsonKey()
  final DateTime endDate;

  @JsonKey(nullable: false)
  final String description;

  Event(this.id, this.name, this.startDate, this.endDate, this.description);

  factory Event.fromJson(Map<String, dynamic> json) => _$EventFromJson(json);

  Map<String, dynamic> toJson() => _$EventToJson(this);
}

@JsonLiteral('../assets/json/events.json')
Map get eventsData => _$EventsDataJsonLiteral;