import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';

// A screen that allows users to take a picture using a given camera.
class Body extends StatefulWidget {
  final List<CameraDescription> cameras;

  const Body({
    Key key,
    @required this.cameras,
  }) : super(key: key);

  @override
  BodyState createState() => BodyState();
}

class BodyState extends State<Body> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  CameraDescription selectedCamera;
  int selectedCameraIndex;

  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      selectedCamera,
      // Define the resolution to use.
      ResolutionPreset.medium,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  void _refresh(CameraDescription camera, int index) {
    debugPrint("Refresh");

    setState(() {
      selectedCamera = camera;
      selectedCameraIndex = index;
      _controller = CameraController(
        // Get a specific camera from the list of available cameras.
        selectedCamera,
        // Define the resolution to use.
        ResolutionPreset.medium,
      );

      // Next, initialize the controller. This returns a Future.
      _initializeControllerFuture = _controller.initialize();
    });
  }

  void _swap() {
    debugPrint("Refresh");
    var index = 0;
    if (selectedCameraIndex == 0) {
      index = 1;
    }
    setState(() {
      selectedCamera = widget.cameras[index];
      selectedCameraIndex = index;
      _controller = CameraController(
        // Get a specific camera from the list of available cameras.
        selectedCamera,
        // Define the resolution to use.
        ResolutionPreset.medium,
      );

      // Next, initialize the controller. This returns a Future.
      _initializeControllerFuture = _controller.initialize();
    });
  }

  Widget _getFloatingActionCancelButton() {
    if (this.selectedCamera != null) {
      return FloatingActionButton(
        heroTag: "btn1",
        child: Icon(Icons.cancel),
        // Provide an onPressed callback.
        onPressed: () async {
          this._refresh(null, null);
        },
      );
    } else {
      return Container();
    }
  }

  Widget _getFloatingActionSwapButton() {
    if (this.selectedCamera != null) {
      return FloatingActionButton(
        heroTag: "btn2",
        child: Icon(Icons.refresh),
        // Provide an onPressed callback.
        onPressed: () async {
          this._swap();
        },
      );
    } else {
      return Container();
    }
  }

  Widget _getFloatingActionCameraButton() {
    if (this.selectedCamera != null) {
      return FloatingActionButton(
        heroTag: "btn3",
        child: Icon(Icons.camera_alt),
        // Provide an onPressed callback.
        onPressed: () async {
          // Take the Picture in a try / catch block. If anything goes wrong,
          // catch the error.
          try {
            // Ensure that the camera is initialized.
            await _initializeControllerFuture;

            // Construct the path where the image should be saved using the
            // pattern package.
            final path = join(
              // Store the picture in the temp directory.
              // Find the temp directory using the `path_provider` plugin.
              (await getTemporaryDirectory()).path,
              '${DateTime.now()}.png',
            );

            // Attempt to take a picture and log where it's been saved.
            await _controller.takePicture(path);
            print(path);
            // If the picture was taken, display it on a new screen.
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(imagePath: path),
              ),
            );
          } catch (e) {
            // If an error occurs, log the error to the console.
            print(e);
          }
        },
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // Wait until the controller is initialized before displaying the
        // camera preview. Use a FutureBuilder to display a loading spinner
        // until the controller has finished initializing.
        body: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot) {
            if (this.selectedCamera != null) {
              if (snapshot.connectionState == ConnectionState.done) {
                // If the Future is complete, display the preview.
                return CameraPreview(_controller);
              } else {
                // Otherwise, display a loading indicator.
                return Center(child: CircularProgressIndicator());
              }
            } else {
              return CameraSelector(cameras: widget.cameras, refresh: _refresh);
            }
          },
        ),
        floatingActionButton: Padding(
            padding: const EdgeInsets.only(
              left: 20,
              top: 0,
              right: 0,
              bottom: 0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomLeft,
                  child: _getFloatingActionCancelButton(),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: _getFloatingActionSwapButton(),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: _getFloatingActionCameraButton(),
                ),
              ],
            )));
  }
}

class CameraSelector extends StatelessWidget {
  final Function refresh;
  final List<CameraDescription> cameras;
  final _formKey = GlobalKey<FormState>();

  CameraSelector({Key key, this.refresh, this.cameras}) : super(key: key);
  CameraDescription camera;
  int cameraIndex;

  void cameraHandler() {
    refresh(camera, cameraIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 40,
            top: 20,
            right: 40,
            bottom: 20,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("Select camera", style: TextStyle(fontSize: 22)),
                SizedBox(height: 40),
                DropdownButtonFormField<List>(
                  items: this.cameras.asMap().entries.map((entry) {
                    return new DropdownMenuItem<List>(
                      value: [entry.value, entry.key],
                      child: new Text(entry.value.name),
                    );
                  }).toList(),
                  validator: (value) {
                    if (value == null) {
                      return 'Please select camera';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Select camera',
                  ),
                  onChanged: (item) {
                    this.camera = item[0];
                    this.cameraIndex = item[1];
                  },
                ),
                SizedBox(height: 20),
                RaisedButton(
                  onPressed: cameraHandler,
                  child: Text('Open selected camera',
                      style: TextStyle(color: Colors.white)),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Display the Picture')),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Image.file(File(imagePath)),
    );
  }
}
