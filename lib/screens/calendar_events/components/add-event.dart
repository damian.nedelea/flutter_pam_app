import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

// Create a Form widget.
class EventsList extends StatefulWidget {
  @override
  EventsListState createState() {
    return EventsListState();
  }
}

class EventsListState extends State<EventsList> {
  final _formKey = GlobalKey<FormState>();
  final searchFieldController = TextEditingController();

  CalendarView selectedNavView = CalendarView.month;

  void handleChangeViewMode(CalendarView view) {
    setState(() {
      selectedNavView = view;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text("Add event"),
    );
  }
}
