import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pam_app/components/navigation_bar_drawer.dart';
import 'components/body.dart';
import 'camera-bloc.dart';
import 'package:flutter_pam_app/bloc/bloc-prov.dart';

class Camera extends StatefulWidget {
  @override
  _CameraState createState() => _CameraState();
}

class _CameraState extends State<Camera> {
  CameraBloc example2Bloc;

  Future<List<CameraDescription>> getCameras() async {
    return await availableCameras();
  }


  @override
  void initState() {
    super.initState();

    example2Bloc = CameraBloc();
  }

  @override
  void dispose() {
    example2Bloc.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: CameraBloc(),
      child: Scaffold(
        appBar: AppBar(
          title: Text("Camera"),
        ),
        body: FutureBuilder<List<CameraDescription>>(
          future: getCameras(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return new Text('Press button to start');
              case ConnectionState.waiting:
                return new Text('Awaiting result...');
              default:
                if (snapshot.hasError)
                  return new Text('Error: ${snapshot.error}');
                else
                  return new Body(
                    cameras: snapshot.data,
                  );
            }
          },
        ),
        drawer: NavigationBarDrawer(),
      ),
    );
  }
}
