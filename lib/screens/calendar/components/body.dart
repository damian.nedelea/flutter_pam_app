import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

// Create a Form widget.
class Body extends StatefulWidget {
  @override
  BodyState createState() {
    return BodyState();
  }
}

class BodyState extends State<Body> {
  final _formKey = GlobalKey<FormState>();
  final searchFieldController = TextEditingController();

  CalendarView selectedNavView = CalendarView.month;

  void handleChangeViewMode(CalendarView view) {
    setState(() {
      selectedNavView = view;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: [
      Padding(
        padding: const EdgeInsets.only(
          left: 20,
          top: 10,
          right: 20,
          bottom: 10,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: RaisedButton(
                child: Text("Month", style: TextStyle(color: Colors.white)),
                onPressed: () {
                  handleChangeViewMode(CalendarView.month);
                },
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: RaisedButton(
                child: Text("Day", style: TextStyle(color: Colors.white)),
                onPressed: () {
                  handleChangeViewMode(CalendarView.day);
                },
              ),
            ),
          ],
        ),
      ),
      Row(
        children: [
          Align(
              alignment: Alignment.center,
              child: SfCalendar(
                view: selectedNavView,
                showNavigationArrow: true,
              )),
        ],
      )
    ]));
  }
}
