part of 'event.dart';

Event _$EventFromJson(Map<String, dynamic> json) {
  return Event(
    json['id'] as String,
    json['name'] as String,
    DateTime.parse(json['startDate'] as String),
    DateTime.parse(json['endDate'] as String),
    json['description'] as String,
  );
}

Map<String, dynamic> _$EventToJson(Event instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('startDate', instance.startDate.toIso8601String());
  writeNotNull('endDate', instance.startDate.toIso8601String());
  writeNotNull('description', instance.description);

  return val;
}

final _$EventsDataJsonLiteral = {};