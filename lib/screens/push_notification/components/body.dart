import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// Create a Form widget.
class Body extends StatefulWidget {
  @override
  BodyState createState() {
    return BodyState();
  }
}

class BodyState extends State<Body> {
  final _formKey = GlobalKey<FormState>();
  final textController = TextEditingController();
  final timeController = TextEditingController();

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  void showNotificationHandler() async {
    if (_formKey.currentState.validate()) {

      print(int.parse(timeController.text));
      var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'Channel ID',
        'Channel name',
        'Channel description',
        priority: Priority.Max,
        timeoutAfter: int.parse(timeController.text) * 1000,
        importance: Importance.Max,
        icon: '@mipmap/ic_launcher',
        ticker: "test",
        largeIcon: DrawableResourceAndroidBitmap('@mipmap/ic_launcher'),
      );
      var iOSPlatformChannelSpecifics = IOSNotificationDetails();
      var platformChannelSpecifics = NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
          0,
          'New Notification',
          textController.text,
          platformChannelSpecifics);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(
          left: 40,
          top: 20,
          right: 40,
          bottom: 20,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text("Push notification", style: TextStyle(fontSize: 22)),
              SizedBox(height: 40),
              TextFormField(
                controller: textController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Notification text',
                ),
              ),
              SizedBox(height: 20),
              TextFormField(
                controller: timeController,
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly
                ],
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter number';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  suffixText: 'Sec.',
                  border: OutlineInputBorder(),
                  labelText: 'Time',
                ),
              ),
              SizedBox(height: 20),
              RaisedButton(
                onPressed: showNotificationHandler,
                child: Text('Show notification',
                    style: TextStyle(color: Colors.white)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
