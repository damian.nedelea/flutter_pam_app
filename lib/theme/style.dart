import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
    primaryColor: Colors.blueAccent,
    accentColor: Colors.orange,
    hintColor: Colors.grey,
    dividerColor: Colors.grey,
    buttonColor: Colors.blueAccent,
    scaffoldBackgroundColor: Colors.white,
    canvasColor: Colors.white,
  );
}